<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//API for Customer
$router->post('/register', 'CustomerController@register');
$router->get('/login', 'CustomerController@login');
$router->get('/getUserData', 'CustomerController@getUserData');
$router->put('/updateUserData', 'CustomerController@updateUserData');
$router->get('/forgotPassword', 'CustomerController@forgotPassword');
$router->put('/changePassword', 'CustomerController@changePassword');
$router->put('/changePassword2', 'CustomerController@changePassword2');
$router->post('/order', 'CustomerController@order');
$router->get('/getOrder', 'CustomerController@getOrder');
$router->get('/getOrders', 'CustomerController@getOrders');
$router->get('/getLaundries', 'CustomerController@getLaundries');
$router->get('/getPackets', 'CustomerController@getPackets');
$router->get('/getLocations', 'CustomerController@getLocations');
$router->get('/getServices', 'CustomerController@getServices');
$router->put('/cancelOrder', 'CustomerController@cancelOrder');
$router->put('/updateBalance', 'CustomerController@updateBalance');
$router->get('/getBalance', 'CustomerController@getBalance');
$router->put('/payOrder', 'CustomerController@payOrder');

//API for Admin
$router->post('/registerAdmin', 'AdminController@register');
$router->get('/loginAdmin', 'AdminController@login');
$router->get('/getBalanceAdmin', 'AdminController@getBalance');
$router->get('/getDataAdmin', 'AdminController@getDataAdmin');
$router->put('/updateAdminData', 'AdminController@updateAdminData');
$router->put('/changePasswordAdmin', 'AdminController@changePassword');
$router->put('/updateOrder', 'AdminController@updateOrder');
$router->get('/getIncomingOrders', 'AdminController@getIncomingOrders');
$router->get('/getPickupOrders', 'AdminController@getPickupOrders');
$router->get('/getProcessOrder', 'AdminController@getProcessOrder');
$router->get('/getDeliverOrder', 'AdminController@getDeliverOrder');
