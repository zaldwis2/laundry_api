<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Admins;
use App\Models\Orders;
use App\Models\Laundries;
use App\Models\Packets;
use App\Models\Locations;
use App\Models\Services;

class AdminController extends Controller
{
    public function register(Request $request){
        try {
            $dataUser=Admins::where('email','=',$request->email)->first();
            if($dataUser){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Email Already Exist'
                ];    
            } else {
                $registerLaundry = new Laundries();
                $registerLaundry->name = $request->laundry_name;
                $registerLaundry->saveOrFail();

                $registerUser = new Admins();
                $registerUser->email = $request->email;
                $registerUser->name = $request->name;
                $registerUser->phone = $request->phone;
                $registerUser->laundry_id = $registerLaundry->id;
                $registerUser->password = Hash::make($request->password);
                $registerUser->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Register Complete'
                ]; 
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Register Failed'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function login(Request $request)
    {
        $password = $request->password;
        try{
            $query=Admins::where('email','=',$request->email)->first();
            if(!$query){
                $statusCode = 404;                    
                $response = [
                    'error' => true,
                    'message' => 'Email Doesnt Exist'
                ];      
            } else {
                $dataPassword = $query->password;
                if(Hash::check($password, $dataPassword)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Success',
                        'dataAdmin' => $query
                    ];    
                } else {
                    $statusCode = 404;                    
                    $response = [
                        'error' => true,
                        'message' => 'Wrong Password or Email'
                    ];       
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Failed';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getBalance(Request $request){
        try{
            $dataUser=Admins::where('id','=',$request->id)->first();

            if($dataUser){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Balance',
                    'balance' => $dataUser->balance
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account'
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getDataAdmin(Request $request){
        try {
            $data = DB::table('admins')
            ->join('laundries', 'admins.laundry_id', '=', 'laundries.id')
            ->select('admins.*','laundries.name AS laundry_name')
            ->where('admins.id', $request->id)
            ->first();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataAdmin' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Account',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateAdminData(Request $request){
        try{
            $dataUser=Admins::where('id','=',$request->id)->first();

            if($dataUser){
                if($request->name != ""){
                    $dataUser->name = $request->name;
                }

                if($request->email != ""){
                    $dataUser->email = $request->email;
                }

                if($request->phone != ""){
                    $dataUser->phone = $request->phone;
                }
                $dataUser->saveOrFail();

                $dataLaundry=Laundries::where('id','=',$request->laundry_id)->first();
                if($dataLaundry){
                    if($request->laundry_name != ""){
                        $dataLaundry->name = $request->laundry_name;
                    }
                    $dataLaundry->saveOrFail();
                }
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Saved',
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Failed',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function changePassword(Request $request){
        try{
            $dataUser=Admins::where('id','=',$request->id)->first();

            if($dataUser){
                if(Hash::check($request->old_password, $dataUser->password)){
                    $dataUser->password = Hash::make($request->new_password);
                    $dataUser->saveOrFail();
            
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Password Saved',
                    ];
                }else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Wrong Password',
                    ];
                }
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateOrder(Request $request){
        try{
            $data=Orders::where('id','=',$request->id)->first();

            if($data){
                if($request->weight != ""){
                    $data->weight = $request->weight;
                }

                if($request->price != ""){
                    $data->price = $request->price;
                }

                if($request->is_accepted != ""){
                    $data->is_accepted = $request->is_accepted;
                }

                if($request->is_picked != ""){
                    $data->is_picked = $request->is_picked;
                }

                if($request->is_processed != ""){
                    $data->is_processed = $request->is_processed;
                }

                if($request->is_delivered != ""){
                    $data->is_delivered = $request->is_delivered;
                }
                $data->saveOrFail();

        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Saved',
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Failed',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getIncomingOrders(Request $request){
        try {
            $data = DB::table('orders')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('services', 'orders.service_id', '=', 'services.id')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->select('orders.*','customers.name AS customer_name',
            'customers.phone AS customer_phone',
            'locations.name AS location_name',
            'services.name AS service_name',
            'packets.name AS packet_name')
            ->where('orders.laundry_id', $request->laundry_id)
            ->where('orders.is_accepted', '0')
            ->orderBy('orders.created_at', 'DESC')
            ->get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataIncomingOrder' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getPickupOrders(Request $request){
        try {
            $data = DB::table('orders')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('services', 'orders.service_id', '=', 'services.id')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->select('orders.*','customers.name AS customer_name',
            'customers.phone AS customer_phone',
            'locations.name AS location_name',
            'services.name AS service_name',
            'packets.name AS packet_name')
            ->where('orders.laundry_id', $request->laundry_id)
            ->where('orders.is_accepted', '1')
            ->where('orders.is_picked', '0')
            ->orderBy('orders.created_at', 'DESC')
            ->get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataPickupOrder' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getProcessOrder(Request $request){
        try {
            $data = DB::table('orders')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('services', 'orders.service_id', '=', 'services.id')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->select('orders.*','customers.name AS customer_name',
            'customers.phone AS customer_phone',
            'locations.name AS location_name',
            'services.name AS service_name',
            'packets.name AS packet_name')
            ->where('orders.laundry_id', $request->laundry_id)
            ->where('orders.is_accepted', '1')
            ->where('orders.is_picked', '1')
            ->where('orders.is_processed', '0')
            ->orderBy('orders.created_at', 'DESC')
            ->get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataProcessOrder' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getDeliverOrder(Request $request){
        try {
            $data = DB::table('orders')
            ->join('customers', 'orders.customer_id', '=', 'customers.id')
            ->join('locations', 'orders.location_id', '=', 'locations.id')
            ->join('services', 'orders.service_id', '=', 'services.id')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->select('orders.*','customers.name AS customer_name',
            'customers.phone AS customer_phone',
            'locations.name AS location_name',
            'services.name AS service_name',
            'packets.name AS packet_name')
            ->where('orders.laundry_id', $request->laundry_id)
            ->where('orders.is_accepted', '1')
            ->where('orders.is_picked', '1')
            ->where('orders.is_processed', '1')
            ->where('orders.is_delivered', '0')
            ->orderBy('orders.created_at', 'DESC')
            ->get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataDeliverOrder' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
}