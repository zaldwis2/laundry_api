<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Customers;
use App\Models\Admins;
use App\Models\Orders;
use App\Models\Laundries;
use App\Models\Packets;
use App\Models\Locations;
use App\Models\Services;

class CustomerController extends Controller
{
    
    public function register(Request $request){
        try {
            $dataUser=Customers::where('email','=',$request->email)->first();
            if($dataUser){
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Email Already Exist'
                ];    
            } else {
                $registerUser = new Customers();
                $registerUser->email = $request->email;
                $registerUser->name = $request->name;
                $registerUser->phone = $request->phone;
                $registerUser->password = Hash::make($request->password);
                $registerUser->saveOrFail();

                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Register Complete'
                ]; 
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Register Failed'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }


    public function login(Request $request)
    {
        $password = $request->password;
        try{
            $query=Customers::where('email','=',$request->email)->first();
            if(!$query){
                $statusCode = 404;                    
                $response = [
                    'error' => true,
                    'message' => 'Email Doesnt Exist'
                ];      
            } else {
                $dataPassword = $query->password;
                if(Hash::check($password, $dataPassword)){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Login Success',
                        'dataUser' => $query
                    ];    
                } else {
                    $statusCode = 404;                    
                    $response = [
                        'error' => true,
                        'message' => 'Wrong Password or Email'
                    ];       
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Failed';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getUserData(Request $request)
    {
        try{
            $query=Customers::where('id','=',$request->id)->first();
            if(!$query){
                $statusCode = 404;                    
                $response['message'] = 'Account Doesnt Exist';
            } else {
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'User Data',
                    'dataUser' => $query
                ];    
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Login Failed';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateUserData(Request $request){
        try{
            $dataUser=Customers::where('id','=',$request->id)->first();

            if($dataUser){
                if($request->name != ""){
                    $dataUser->name = $request->name;
                }

                if($request->email != ""){
                    $dataUser->email = $request->email;
                }

                if($request->phone != ""){
                    $dataUser->phone = $request->phone;
                }

                $dataUser->saveOrFail();
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Data Saved',
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Failed',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function forgotPassword(Request $request)
    {
        try{
            $query=Customers::where('email','=',$request->email)->first();
            if(!$query){
                $statusCode = 404;                    
                $response['message'] = 'Email Doesnt Exist';
            } else {
                if($query->name == $request->name && $query->phone == $request->phone){
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Data Correct',
                        'dataUser' => $query
                    ];    
                }else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Data Incorrect'
                    ];    
                }
            }
        } catch (Exception $ex) {
            $statusCode = 404;
            $response['message'] = 'Error';
        } finally {
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function changePassword(Request $request){
        try{
            $dataUser=Customers::where('id','=',$request->id)->first();

            if($dataUser){
                $dataUser->password = Hash::make($request->password);
                $dataUser->saveOrFail();
        
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Password Saved',
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){

            $statusCode = 404;
            $response = [
                'error' => true,
                'message' => 'Failed',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function changePassword2(Request $request){
        try{
            $dataUser=Customers::where('id','=',$request->id)->first();

            if($dataUser){
                if(Hash::check($request->old_password, $dataUser->password)){
                    $dataUser->password = Hash::make($request->new_password);
                    $dataUser->saveOrFail();
            
                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Password Saved',
                    ];
                }else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Wrong Password',
                    ];
                }
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account',
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function order(Request $request){
        try {
            $order = new Orders();
            $order->customer_id = $request->customer_id;
            $order->location_id = $request->location_id;
            $order->laundry_id = $request->laundry_id;
            $order->service_id = $request->service_id;
            $order->packet_id = $request->packet_id;
            $order->pickup_time = $request->pickup_time;
            $order->finish_time = $request->finish_time;
            $order->saveOrFail();

            $statusCode = 200;
            $response = [
                'error' => false,
                'message' => 'Order Complete'
            ]; 
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Order Failed'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getOrder(Request $request){
        try {
            $data = DB::table('orders')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->join('laundries', 'orders.laundry_id', '=', 'laundries.id')
            ->select('orders.*','packets.name AS packet_name','laundries.name AS laundry_name')
            ->where('orders.customer_id', $request->customer_id)
            ->where('orders.is_delivered', "0")
            ->orderBy('orders.created_at', 'DESC')
            ->first();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataOrder' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getOrders(Request $request){
        try {
            $data = DB::table('orders')
            ->join('packets', 'orders.packet_id', '=', 'packets.id')
            ->join('laundries', 'orders.laundry_id', '=', 'laundries.id')
            ->select('orders.*','packets.name AS packet_name','laundries.name AS laundry_name')
            ->where('orders.customer_id', $request->customer_id)
            ->orderBy('orders.created_at', 'DESC')
            ->get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Data',
                    'dataOrders' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Data Order',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getLaundries(Request $request){
        try {
            $data = Laundries::get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Laundries Data',
                    'dataLaundries' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Laundries',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getPackets(Request $request){
        try {
            $data = Packets::get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Packets Data',
                    'dataPackets' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Packets',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getLocations(Request $request){
        try {
            $data = Locations::get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Locations Data',
                    'dataLocations' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Locations',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getServices(Request $request){
        try {
            $data = Services::get();

            if($data){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Services Data',
                    'dataServices' => $data
                ];    
            } else {
                $statusCode = 404;
                $response = [
                    'error' => false,
                    'message' => 'Cannot Find Services',
                ];    
            }
        } catch (Exception $e) {
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Server error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function cancelOrder(Request $request){
        try{
            $data=Orders::where('id','=',$request->id)->first();

            if($data){
                $data->is_accepted = $request->is_accepted;
                $data->is_picked = $request->is_picked;
                $data->is_processed = $request->is_processed;
                $data->is_delivered = $request->is_delivered;
                $data->saveOrFail();
            
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Order Canceled',
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Order',
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function updateBalance(Request $request){
        try{
            $data=Customers::where('id','=',$request->id)->first();

            if($data){
                $newBalance = $data->balance + $request->balance;
                $data->balance = $newBalance;
                $data->saveOrFail();
            
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Balance Updated',
                    'balance' => $newBalance
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find User',
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error',
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function getBalance(Request $request){
        try{
            $dataUser=Customers::where('id','=',$request->id)->first();

            if($dataUser){
                $statusCode = 200;
                $response = [
                    'error' => false,
                    'message' => 'Balance',
                    'balance' => $dataUser->balance
                ];
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Account'
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }

    public function payOrder(Request $request){
        try{
            $dataOrder=Orders::where('id','=',$request->order_id)->first();
            $dataCustomer=Customers::where('id','=',$request->customer_id)->first();
            $dataAdmin=Admins::where('laundry_id','=',$dataOrder->laundry_id)->first();

            if($dataOrder && $dataCustomer && $dataAdmin){    
                $currentBalance = $dataCustomer->balance;
                $finalBalance = $currentBalance - $dataOrder->price;

                if($finalBalance >= 0){
                    $dataOrder->is_paid = '1';
                    $dataOrder->saveOrFail();

                    $dataCustomer->balance = $finalBalance;
                    $dataCustomer->saveOrFail();

                    $currentAdminBalance = $dataAdmin->balance;
                    $finalAdminBalance = $currentAdminBalance + $dataOrder->price;
                    $dataAdmin->balance = $finalAdminBalance;
                    $dataAdmin->saveOrFail();

                    $statusCode = 200;
                    $response = [
                        'error' => false,
                        'message' => 'Payment Complete',
                        'balance' => $dataCustomer->balance
                    ];
                }else{
                    $statusCode = 404;
                    $response = [
                        'error' => true,
                        'message' => 'Insufficient Balance'
                    ];
                }
            }else{
                $statusCode = 404;
                $response = [
                    'error' => true,
                    'message' => 'Cannot Find Order'
                ];
            }

        }catch (Exception $e){
            $statusCode = 500;
            $response = [
                'error' => true,
                'message' => 'Error'
            ];    
        }finally{
            return response($response,$statusCode)->header('Content-Type','application/json');
        }
    }
}